// Máquina caça níquel
// Preciso de um botâo start
// Preciso que esse botão acione os números aleatórios
// se os números aparecerem iguais na tela uma mensagem de Você ganhou deve aparecer
// se os números forem diferentes uma msg de você perdeu deve aparecer
// Fazer um for para de 0 a 10
// precisa de um for para cada girada
// O primeiro for vai ser um número aleatório e dizer de 1 a 10 sendo cada número o nome de um filme
// O segundo for vai ser um personagem aleatório definido com o for de 1 a 10
// O terceiro for é de 1 a 10 com a foto do personagem, cada número tem uma foto diferente
// fazer botão com -- para esquerda
// botão direita só chamar a função setMovieDiv
// Vai precisar de uma condição de parada

const rollOfDice = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min  
let filmes = [
    {
        genero: "Senhor dos Anéis",
        img : "./Senhor-do-Anéis/Title.webp",
        personagen: [
            { nome: "Frodo", img : "./Senhor-do-Anéis/Frodo.jpg" },
            { nome: "Bilbo", img : "./Senhor-do-Anéis/Bilbo.jpg" },
            { nome: "Legolas", img : "./Senhor-do-Anéis/Legolas.jpg" },
            { nome: "Gandalf", img : "./Senhor-do-Anéis/Gandalf.jpg" },
            { nome: "Saroman", img : "./Senhor-do-Anéis/saruman.jpg" },
            { nome: "Peregrin Took", img : "./Senhor-do-Anéis/Peregrin.jpg" },
            { nome: "Boromir", img : "./Senhor-do-Anéis/Boromir.png" },
            { nome: "Sam", img : "./Senhor-do-Anéis/Sam.jpg" },
            { nome: "Gimili", img : "./Senhor-do-Anéis/Gimili.jpg" },
            { nome: "Merry", img : "./Senhor-do-Anéis/Merri.jpg" },
        ],   
    },

    {
        genero: "Harry Potter", 
        img : "./Harry-Potter/Title.webp",
        personagen: [
            { nome: "Harry", img : "./Harry-Potter/Harry.jpg" }, 
            { nome: "Hemione", img : "./Harry-Potter/Hermione.jpg" },
            { nome: "Weslley", img : "./Harry-Potter/Weslley.jpg" },
            { nome: "Voldemort", img : "./Harry-Potter/Voldemort.jpg" },
            { nome: "Hagrid", img : "./Harry-Potter/Hagrid.jpg" },
            { nome: "Dumbledore", img : "./Harry-Potter/dumbledore.jpg" },
            { nome: "Severo Snap", img : "./Harry-Potter/Severo.jpg" },
            { nome: "Gina", img : "./Harry-Potter/Gina.jpg" },
            { nome: "Malfoy", img : "./Harry-Potter/Draco.png" },
            { nome: "Olho-Tonto", img : "./Harry-Potter/OlhoTonto.jpg" },
        ]
    },

    {
        genero: "O hobbit", 
        img : "./O-hobbit/Title.jpg",
        personagen: [
            { nome: "Legolas", img : "./O-hobbit/Legolas.jpg" },
            { nome: "Thorin", img : "./O-hobbit/Thorin.jpg" },
            { nome: "Bilbo", img : "./O-hobbit/bilbo.jpg" },
            { nome: "Smeagol", img : "./O-hobbit/Smeagol.jpg" },
            { nome: "Frodo", img : "./O-hobbit/Frodo.jpg" },
            { nome: "Tauriel", img : "./O-hobbit/Tauriel.jpg" },
            { nome: "Sauron", img : "./O-hobbit/Sauron.jpg" },
            { nome: "Smaug", img : "./O-hobbit/Smaug.jpg" },
            { nome: "Elrond", img : "./O-hobbit/Elrond.jpg" },
            { nome: "Azog", img : "./O-hobbit/Azog.jpg" },
        ]
    },

    {
        genero: "Game of Thrones",
        img : "./Game-of-Thrones/Title.jpg",
        personagen: [
            { nome: "Sansa Stark", img : "./Game-of-Thrones/sansastark.jpg" },
            { nome: "Ramsay Snow", img : "./Game-of-Thrones/Ramsay.jpg"},
            { nome: "Tyrion", img : "./Game-of-Thrones/Tyrion.jpg" },
            { nome: "Arya Stark", img : "./Game-of-Thrones/Aryastark.jpg" },
            { nome: "Daenerys Targaryen", img : "./Game-of-Thrones/DaenerysTargaryen.jpg" },
            { nome: "Cersei Lanninster", img : "./Game-of-Thrones/Cersei.png" },
            { nome: "Robb Stark", img : "./Game-of-Thrones/Robbstark.jpg" },
            { nome: "Bran Stark", img : "./Game-of-Thrones/Branstark.png" },
            { nome: "Rei da Noite", img : "./Game-of-Thrones/Reidanoite.jpg" },
            { nome: "Theon Greyjoy", img : "./Game-of-Thrones/Theon.jpg" },
        ]
    },

    {
        genero: "Naruto", 
        img : "./Naruto/Title.jpeg",
        personagen: [
            { nome: "Naruto", img : "./Naruto/naruto.jpg" },
            { nome: "Sakura", img : "./Naruto/Sakura.png" },
            { nome: "Kakashi", img : "./Naruto/Kakashi.jpg" },
            { nome: "Sasuke", img : "./Naruto/sasuke.jpg" },
            { nome: "Itachi", img : "./Naruto/Itachi.jpg" },
            { nome: "Pain", img : "./Naruto/pain.jpg" },
            { nome: "Madara", img : "./Naruto/Madara.jpg" },
            { nome: "Obito", img : "./Naruto/Obito.jpg" },
            { nome: "Lee", img : "./Naruto/Lee.jpeg" },
            { nome: "Hinata", img : "./Naruto/Hinata.jpg" },
        ]
    },

    {
        genero: "Vingadores", 
        img : "./Vingadores/Title.jpg",
        personagen: [
            { nome: "Capitão América", img : "./Vingadores/Capitaoamerica.jpeg" },
            { nome: "Homem de Ferro", img : "./Vingadores/Homemdeferro.jpg" },
            { nome: "Pantera Negra", img : "./Vingadores/panteranegra.jpg" },
            { nome: "Hulk", img : "./Vingadores/Hulk.jpg" },
            { nome: "Thor", img : "./Vingadores/thor.jpg" },
            { nome: "Visão", img : "./Vingadores/visão.jpg.jpeg" },
            { nome: "Feiticeira", img : "./Vingadores/Feiticeira.jpg" },
            { nome: "Gavião Arqueiro", img : "./Vingadores/gaviaoarqueiro.jpg.jpeg" },
            { nome: "Homem Formiga", img : "./Vingadores/Homemformiga.jpg" },
            { nome: "Homem Aranha", img : "./Vingadores/homemaranha.jpg" },
        ]
    },

    {
        genero: "Liga da Justiça", 
        img : "./Liga-da-Justiça/Title.jpg",
        personagen: [
            { nome: "Batman", img : "./Liga-da-Justiça/Batman.jpg" },
            { nome: "Mulher Maravilha", img : "./Liga-da-Justiça/MulherMaravilha.jpg"},
            { nome: "Superman", img : "./Liga-da-Justiça/superman.jpg.jpeg" },
            { nome: "Flash", img : "./Liga-da-Justiça/Flash.jpg.jpeg"},
            { nome: "Aquaman", img : "./Liga-da-Justiça/aquaman.jpg" },
            { nome: "Robin", img : "./Liga-da-Justiça/Robin.jpg" },
            { nome: "Lanterna Verde", img : "./Liga-da-Justiça/lanternaverde.jpg"},
            { nome: "Lex luthor", img : "./Liga-da-Justiça/Lexlutor.jpg" },
            { nome: "Victor Stone", img : "./Liga-da-Justiça/cyborg.jpg" },
            { nome: "Slade", img : "./Liga-da-Justiça/Slade.jpg" },
        ]
    },

    {
        genero: "League of Legends", 
        img : "./League-of-Legends/Title.jpg",
        personagen: [
            { nome: "Lux", img : "./League-of-Legends/Lux.jpg" },
            { nome: "Ezreal", img : "./League-of-Legends/Ezreal.jpg" },
            { nome: "Katarina", img : "./League-of-Legends/Katarina.jpg" },
            { nome: "Gnar", img : "./League-of-Legends/Gnar.jpg" },
            { nome: "Le Blanc", img : "./League-of-Legends/Leblanc.jpg" },
            { nome: "Ahri", img : "./League-of-Legends/Ahri.jpg" },
            { nome: "Miss Fortune", img : "./League-of-Legends/MissFortune.jpg" },
            { nome: "Master Yi", img : "./League-of-Legends/MasterYi.jpg" },
            { nome: "Annie", img : "./League-of-Legends/Annie.jpg" },
            { nome: "Kai Sah", img : "./League-of-Legends/Kaisa.jpg" },
        ]
    },

    {
        genero: "Velozes e furiosos", 
        img : "./Velozes/Title.webp",
        personagen: [
            { nome: "Toreto", img : "./Velozes/Toreto.jpg" },
            { nome: "Brian", img : "./Velozes/Brian.jpg" },
            { nome: "Mia", img : "./Velozes/Mia.jpg" },
            { nome: "Letty", img : "./Velozes/Letty.jpg" },
            { nome: "Roman", img : "./Velozes/Roman.jpg" },
            { nome: "Megan", img : "./Velozes/Megan.jpg" },
            { nome: "Deckard", img : "./Velozes/deckard.jpg" },
            { nome: "Gisele", img : "./Velozes/Gisele.jpg" },
            { nome: "Luke", img : "./Velozes/Luke.jpg" },
            { nome: "Tej", img : "./Velozes/tej.jpg" },
        ]
    },

    {
        genero: "X-men",
        img : "./X-men/Title.jpg",
        personagen: [
            { nome: "Mercurio", img : "./X-men/Mercurio.jpg" },
            { nome: "Wolverine", img : "./X-men/Wolverine.jpg" },
            { nome: "Xavier", img : "./X-men/xavier.jpg" },
            { nome: "Deadpool", img : "./X-men/deadpool.jpg" },
            { nome: "Noturno", img : "./X-men/Noturno.jpg" },
            { nome: "Tempestade", img : "./X-men/tempestade.jpg" },
            { nome: "Scott", img : "./X-men/Scott.jpg.jpeg" },
            { nome: "Magneto", img : "./X-men/Magneto.jpg" },
            { nome: "Jean Grey", img : "./X-men/Jeangrey.jpg.jpeg" },
            { nome: "Colossus", img : "./X-men/colossus.jpg" },
        ]
    },
]

// const movie = ['Senhor dos Anéis', 'Harry Potter', 'O Hobbit', 'Game of Thrones', 'Naruto', 'Vingadores', '']
let movie = document.getElementById('movie')
let hero = document.getElementById('personagem')

const createCard = (contentTitle, src) => {
    console.log(movieindex)
    console.log(contentTitle)
    console.log(src)
    const title = document.createElement('h1')
    const img = document.createElement('img')
    img.src = src
    img.className = 'movieImg'
    title.innerHTML = contentTitle
    movie.appendChild(title)
    movie.appendChild(img)
}

const personagem = []
let movieindex = 0
createCard(filmes[movieindex].genero, filmes[movieindex].img)
const nextMovieDiv = () =>{
    movie.innerText = ""
    if (movieindex !== filmes.length -1){
        movieindex++
    }
    createCard(filmes[movieindex].genero, filmes[movieindex].img)
}


const lastMovieDiv = () =>{
    console.log(movieindex)
    movie.innerText = ""
    if (movieindex !== 0) {
        movieindex--
    }
    createCard(filmes[movieindex].genero, filmes[movieindex].img)
}

const personagemDiv = (contentName, src) => {
    const titleHero = document.createElement('h1')
    const imgHero = document.createElement('img')
    imgHero.src = src
    imgHero.className = 'heroImg'
    titleHero.innerHTML = contentName
    hero.appendChild(titleHero)
    hero.appendChild(imgHero)
}
const nextHeroDiv = () =>{
    hero.innerText = ""
    if (movieindex !== filmes.length -1){
        movieindex++
    }
    personagemDiv(filmes[movieindex].personagen[0].nome, filmes[movieindex].personagen[0].img)
} 

const leftHeroDiv = () =>{
    hero.innerText = ""
    if (movieindex !== 0) {
        movieindex--
    }
    personagemDiv(filmes[movieindex].personagen[0].nome, filmes[movieindex].personagen[0].img)
}

const selectMovieDiv = () =>{
    createCard(filmes[movieindex].personagen[0].nome, filmes[movieindex].personagen[0].img)
}

const rightClick1 = document.getElementById('Button2')
rightClick1.addEventListener('click', nextMovieDiv)

const leftClick1 = document.getElementById('Button')
leftClick1.addEventListener('click', lastMovieDiv)

const selectHero = document.getElementById('ButtonStart')
selectHero.addEventListener('click', personagemDiv) //chama a função que tu criar que vai pegar o filme



const createImg = (src) => {
    let img = document.createElement("img")
    img.src = src
    img.className = 'filmes'
    document.body.appendChild(img)
}
function styleimg () {
}

let SA = filmes[1].personagen
SA.forEach(personagem => createImg(personagem.img)) 
let filmPosition = Math.floor(Math.random() + filmes.length + 1)
//let personagemNoIndice = Math.floor(Math.random() + filmes[filmPosition].personagen.length + 1)
//console.log(filmes.personagens[0].nome)

function VerificadorFilme () {
    let arr = []
    for (let i = 0; i <= 10; i++) {
        arr.push(i)
    }
    return arr
}
console.log(VerificadorFilme())

function VerificadorPersonagem (VerificadorFilme) {
    let arr1 = []
    for (let j = 0; j < VerificadorFilme; j++) {
        arr1.push(j)
    }
    return arr1
}
console.log(VerificadorPersonagem())

function rolarDados (num) {
    const rollOfDice = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min
    let count = []
    let msg = ''
        for (let x = 0; x <= 1; x++) {
            count[x] = 0
        }
        for (let i = 0; i <= num; i++) {
        
        let dados1 = rollOfDice(1, 10)
        console.log('number: ' + dados1)
            
        let dados2 = rollOfDice(1, 10)
        console.log('number: ' + dados2)
        if (dados1 > dados2) {
            msg = 'jogador 1 Venceu'
            return msg
        }
        else if (dados1 === dados2){
            msg = 'Empate'
            return msg
        }
        else {
            msg = 'jogador 2 Venceu'
            return msg
        }
    }
    return count
}
console.log(rolarDados(1))


function desabilita (match) {
    match.preventDefault()
    match.stopImmediatePropagation()

    const submitButton = document.querySelector('#countButton input[type="submit"]')
    submitButton.disabled = true
    return false
}

const button = document.getElementById('Button')
button.addEventListener('click', rolarDados)


